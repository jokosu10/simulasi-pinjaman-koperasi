## Cara deploy aplikasi ke server

NB : pastikan php versi yang berjalan minimal 7.2

1. clone project
2. ketikkan `composer install` untuk install dependency
3. ketikkan `composer dump-autoload` untuk publish file helper
4. setting konfigurasi database di file `.env`
5. jalankan perintah berikut untuk membuat database & mengisi database `php artisan migrate --seed`
6. login ke aplikasi dengan credential berikut : <br/> username : admin@sakti.id <br/>
password : 12345678
7. klik tulisan Simulasi untuk memulai simulasi pinjaman.
