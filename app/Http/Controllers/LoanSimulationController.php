<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoanSimulationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('simulation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFlat()
    {
        $valueLoan = 0;
        $bankInterest = 0;
        $installments = 1;
        $angsuranBunga = 0;
        $angsuranPokok = 0;
        $totalAngsuran = 0;
        $sisaAngsuran = 0;

        return view('simulation.create-flat', [
            'valueLoan' => $valueLoan,
            'installments' => $installments,
            'bankInterest' => $bankInterest,
            'angsuranBunga' => $angsuranBunga,
            'angsuranPokok' => $angsuranPokok,
            'totalAngsuran' => $totalAngsuran,
            'sisaAngsuran' => $sisaAngsuran,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeFlat(Request $request)
    {
        $valueLoan = $request->input('valueLoan');
        $bankInterest = $request->input('bankInterest');
        $installments = $request->input('installments');
        //$typeLoan = $request->input('typeLoan');

        // perhitungan bunga flat
        $angsuranPokok = $valueLoan/$installments;
        $angsuranBunga = ($valueLoan*$bankInterest) / 100;
        $totalAngsuran = $angsuranPokok + $angsuranBunga;
        $totalAngsuranRound = round($totalAngsuran, -2);

        return view('simulation.result-flat', [
            'valueLoan' => $valueLoan,
            'installments' => $installments,
            'bankInterest' => $bankInterest,
            'angsuranBunga' => $angsuranBunga,
            'angsuranPokok' => $angsuranPokok,
            'totalAngsuran' => $totalAngsuran,
            'totalAngsuranRound' => $totalAngsuranRound
        ]);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMenurun()
    {
        $valueLoan = 0;
        $bankInterest = 0;
        $installments = 1;
        $angsuranBunga = 0;
        $angsuranPokok = 0;
        $totalAngsuran = 0;

        return view('simulation.create-menurun', [
            'valueLoan' => $valueLoan,
            'installments' => $installments,
            'bankInterest' => $bankInterest,
            'angsuranBunga' => $angsuranBunga,
            'angsuranPokok' => $angsuranPokok,
            'totalAngsuran' => $totalAngsuran,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMenurun(Request $request)
    {
        $valueLoan = $request->input('valueLoan');
        $bankInterest = $request->input('bankInterest');
        $installments = $request->input('installments');
        //$typeLoan = $request->input('typeLoan');

        // perhitungan bunga menurun
        $angsuranPokok = $valueLoan/$installments;
        //$angsuranBunga = ($valueLoan*$bankInterest) / 100;
        //$totalAngsuran = $angsuranPokok + $angsuranBunga;

        return view('simulation.result-menurun', [
            'valueLoan' => $valueLoan,
            'installments' => $installments,
            'bankInterest' => $bankInterest,
            //'angsuranBunga' => $angsuranBunga,
            'angsuranPokok' => $angsuranPokok,
            //'totalAngsuran' => $totalAngsuran,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createAnuitas()
    {
        $valueLoan = 0;
        $bankInterest = 0;
        $installments = 1;
        $angsuranBunga = 0;
        $angsuranPokok = 0;
        $totalAngsuran = 0;

        return view('simulation.create-anuitas', [
            'valueLoan' => $valueLoan,
            'installments' => $installments,
            'bankInterest' => $bankInterest,
            'angsuranBunga' => $angsuranBunga,
            'angsuranPokok' => $angsuranPokok,
            'totalAngsuran' => $totalAngsuran,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAnuitas(Request $request)
    {
        $valueLoan = $request->input('valueLoan');
        $bankInterest = $request->input('bankInterest');
        $installments = $request->input('installments');
        //$typeLoan = $request->input('typeLoan');

        // perhitungan bunga anuitas
        //$angsuranPokok = $valueLoan/$installments;
        //$angsuranBunga = ($valueLoan*$bankInterest) / 100;
        //$totalAngsuran = $angsuranPokok + $angsuranBunga;

        return view('simulation.result-anuitas', [
            'valueLoan' => $valueLoan,
            'installments' => $installments,
            'bankInterest' => $bankInterest,
            //'angsuranBunga' => $angsuranBunga,
            //'angsuranPokok' => $angsuranPokok,
            //'totalAngsuran' => $totalAngsuran,
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
