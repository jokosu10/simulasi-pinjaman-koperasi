@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hasil Simulasi Pinjaman Bunga Anuitas</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Angsuran ke</th>
                                <th scope="col">Sisa Pinjaman</th>
                                <th scope="col">Angsuran Pokok</th>
                                <th scope="col">Bunga Per Bulan</th>
                                <th scope="col">Total Angsuran</th>
                                <th scope="col">Pembulatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $hutang = $valueLoan;
                            @endphp
                            @for ($i = 1; $i <= $installments; $i++)
                                @php
                                    $anuitas = bungaAnuitas($valueLoan, $installments, $bankInterest);
                                    $ang_bunga = $hutang*($bankInterest/100);
                                    $ang_pokok = $anuitas - $ang_bunga;
                                    $hutang = $hutang - $ang_pokok;
                                    $cicilan = $ang_bunga+$ang_pokok;
                                    $cicilanRound = round($cicilan, -2);
                                @endphp
                                @if ($i === 1)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ formatNumber($hutang, 'IDR') }}</td>
                                        <td>{{ formatNumber($ang_pokok, 'IDR') }}</td>
                                        <td>{{ formatNumber($ang_bunga, 'IDR') }}</td>
                                        <td>{{ formatNumber($cicilan, 'IDR') }}</td>
                                        <td>{{ formatNumber($cicilanRound, 'IDR') }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ formatNumber($hutang, 'IDR') }}</td>
                                        <td>{{ formatNumber($ang_pokok, 'IDR') }}</td>
                                        <td>{{ formatNumber($ang_bunga, 'IDR') }}</td>
                                        <td>{{ formatNumber($cicilan, 'IDR') }}</td>
                                        <td>{{ formatNumber($cicilanRound, 'IDR') }}</td>
                                    </tr>
                                @endif
                            @endfor
                        </tbody>
                    </table>
                    <button class="btn btn-primary" onclick="window.location='{{ route('simulation.index') }}'" type="button">Kembali</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
