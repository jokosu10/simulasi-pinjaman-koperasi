@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hasil Simulasi Pinjaman Bunga Flat</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Angsuran ke</th>
                                <th scope="col">Sisa Pinjaman</th>
                                <th scope="col">Angsuran Pokok</th>
                                <th scope="col">Bunga Per Bulan</th>
                                <th scope="col">Total Angsuran</th>
                                <th scope="col">Pembulatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for ($i = 1; $i <= $installments; $i++)
                                @if ($i === 1)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ formatNumber($valueLoan, 'IDR') }}</td>
                                        <td>{{ formatNumber($angsuranPokok, 'IDR') }}</td>
                                        <td>{{ formatNumber($angsuranBunga, 'IDR') }}</td>
                                        <td>{{ formatNumber($totalAngsuran, 'IDR') }}</td>
                                        <td>{{ formatNumber($totalAngsuranRound, 'IDR') }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ formatNumber($valueLoan -= $angsuranPokok,'IDR') }}</td>
                                        <td>{{ formatNumber($angsuranPokok,'IDR') }}</td>
                                        <td>{{ formatNumber($angsuranBunga,'IDR') }}</td>
                                        <td>{{ formatNumber($totalAngsuran,'IDR') }}</td>
                                        <td>{{ formatNumber($totalAngsuranRound, 'IDR') }}</td>
                                    </tr>
                                @endif
                            @endfor
                        </tbody>
                    </table>
                    <button class="btn btn-primary" onclick="window.location='{{ route('simulation.index') }}'" type="button">Kembali</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
