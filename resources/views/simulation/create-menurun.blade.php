@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Simulasi</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('simulation.menurun.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="valueLoan">Nilai Pinjaman</label>
                            <input type="text" class="form-control" id="valueLoan" name="valueLoan" placeholder="Nilai Pinjaman">
                        </div>
                        <div class="form-group">
                            <label for="bankInterest">Bunga Per Bulan (Dalam %)</label>
                            <input type="text" class="form-control" id="bankInterest" name="bankInterest" placeholder="Bunga Per Bulan Dalam Persentase">
                        </div>
                        <div class="form-group">
                            <label for="installments">Angsuran</label>
                            <input type="text" class="form-control" id="installments" name="installments" placeholder="Besarnya cicilan (dalam hitungan bulan)">
                        </div>
                        <button type="submit" class="btn btn-primary">Cek</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
