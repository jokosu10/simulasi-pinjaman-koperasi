@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Simulasi</div>

                <div class="card-body">
                    <button class="btn btn-primary" onclick="window.location='{{ route('simulation.flat.create') }}'" type="button">Silakan Lakukan Simulasi Perhitungan Bunga Flat</button>
                    <button class="btn btn-primary" onclick="window.location='{{ route('simulation.menurun.create') }}'" type="button">Silakan Lakukan Simulasi Perhitungan Bunga Menurun</button>
                    <button class="btn btn-primary" onclick="window.location='{{ route('simulation.anuitas.create') }}'" type="button">Silakan Lakukan Simulasi Perhitungan Bunga Anuitas</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
