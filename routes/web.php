<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('simulation')->group( function() {
    Route::get('/', 'LoanSimulationController@index')->name('simulation.index');
    Route::get('/createFlat', 'LoanSimulationController@createFlat')->name('simulation.flat.create');
    Route::post('/storeFlat', 'LoanSimulationController@storeFlat')->name('simulation.flat.store');
    Route::get('/createMenurun', 'LoanSimulationController@createMenurun')->name('simulation.menurun.create');
    Route::post('/storeMenurun', 'LoanSimulationController@storeMenurun')->name('simulation.menurun.store');
    Route::get('/createAnuitas', 'LoanSimulationController@createAnuitas')->name('simulation.anuitas.create');
    Route::post('/storeAnuitas', 'LoanSimulationController@storeAnuitas')->name('simulation.anuitas.store');
    Route::get('/result', 'LoanSimulationController@index')->name('simulation.index');
});
